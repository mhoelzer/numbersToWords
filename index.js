function onesWords(digit) {
    const ones = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    // first "" is for zero
    return ones[digit];
}

function teenersWords(digit) {
    const teeners = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
    // first "" is for zero
    return teeners[digit];
}

function tensWords(digit) {
    const tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    // for less than twenty
    return tens[digit];
}

function hundredsWords(digit) {
    const hundreds = ["", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred"];
    // first "" is for zero
    // const hundreds = ["one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred", "ten hundred", "eleven hundred", "twelve hundred", "thirteen hundred", "fourteen hundred", "fifteen hundred", "sixteen hundred", "seventeen hundred", "eighteen hundred", "nineteen hundred"];
    return hundreds[digit];
}

for(let i = 1; i <= 1000; i++) {
    const paragraph = document.createElement("p");
    document.body.appendChild(paragraph);
    if(i < 10) {
        // dont need the [] around i because it's like doing [1,2,3] + "dsf" = "1,2,3dsf"
        paragraph.textContent = i + " => " + onesWords(i);
    } else if(i < 20) {
        // just i is < 10 because doing somethi ng similar to above, but the numbers are greater than 10
        paragraph.textContent = i + " => " + teenersWords(i - 10);
    } else if(i < 100) {
        const [tensPlace, onesPlace] = String(i).split("");
        paragraph.textContent = i + " => " + tensWords(tensPlace) + "-" + onesWords(onesPlace);
        // if ones is empty, there is no hyphen like above
        if(onesWords(onesPlace) === "") {
            paragraph.textContent = i + " => " + tensWords(tensPlace) + " " + onesWords(onesPlace);
        }
    } else if(i < 1000) {
        // array destructuring 
        const [hundredsPlace, tensPlace, onesPlace] = String(i).split("");
        if(tensPlace == 1 ) {
            paragraph.textContent = i + " => " + hundredsWords(hundredsPlace) + " " + teenersWords(Number(onesPlace))
        } else if(onesWords(onesPlace) === "" || tensWords(tensPlace) === "") {
            paragraph.textContent = i + " => " + hundredsWords(hundredsPlace) + " " + tensWords(tensPlace) + " " + onesWords(onesPlace);
        } else {
            paragraph.textContent = i + " => " + hundredsWords(hundredsPlace) + " " + tensWords(tensPlace) + "-" + onesWords(onesPlace);
        }
    } else if(i == 1000){
        paragraph.textContent = i + " => " + "one thousand";
    } else {
        paragraph.textContent = "";
    }
}